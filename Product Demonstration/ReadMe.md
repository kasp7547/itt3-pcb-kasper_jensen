# Product Demonstration

This folder contains all of the information pertaining to the product 
demonstration, which is scheduled for December 13th, 2019 at UCL Erhvervsakademi
og Professionshøjskole in Odense, between 9:00 AM and 11:30 AM.

The demonstration takes place in the E-Lab in Building A (room A1.18).

***Project Status***
At the moment, the product does not work. A concept-demo version was showcased
during the presentation. The full product will be developed.