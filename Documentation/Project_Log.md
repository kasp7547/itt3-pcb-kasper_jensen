# Project Log
This project log will be updated every day when work is done on the project. This is not limited to Tuesdays and Wednesdays.

The following date-format will be used for all entries:
Day, DD-MM-YYYY

## Tuesday, 29-10-2019
Summary of Activities:
* Created the project on Gitlab
* Set up the first couple of issues that need to be worked on
* Created the project log
* Created a Block Diagram to give a quick overview of the product
* Started creating a schematic diagram

Lessons Learned:
* Setting up a Gitlab project is more work than first anticipated

Resources Used:
* None yet

## Wednesday, 30-10-2019
Summary of Activities:
* Continued work on schematic
* Found, downloaded and imported some part-symbols into OrCAD Capture CIS
* Drew schematic so far in OrCAD

Lessons Learned:
* How to download and import symbols into OrCAD Capture CIS
* The importance of orderliness in a schematic

Resources Used:
* [UltraLibrarian](https://app.ultralibrarian.com)
* [SnapEDA](https://www.snapeda.com)

## Monday, 04-11-2019
Summary of Activities:
* Installed OrCAD Version 17.4
* Added Student License to OrCAD 17.4
* Added more issues

Lessons Learned:
* OrCAD 17.4 is apparently not officially available to students through NordCAD yet. Regardless, I contacted their customer support and they were happy to provide me with a new Student License that works for version 17.4.
 
Resources Used:
* [NordCAD](https://www.nordcad.dk)

## Tuesday, 05-11-2019
Summary of Activities:
* Created padstacks, symbols and footprints for missing components
* Finished schematic and organized it into smaller parts
* Exported netlist and BOM
* Uploaded OrCAD files to "OrCAD files" folder

Lessons Learned:
* How to create custom padstacks, symbols and footprints
* How to troubleshoot a schematic when creating Netlist returns errors
* How to manually assign PCB Footprints to symbols

Resources Used:
* Nordcad Capture CIS Introduction (book)
* Nordcad PCB Editor introduktion (book)
* [UltraLibrarian](https://app.ultralibrarian.com)

## Wednesday, 06-11-2019
Summary of Activities:
* Double-checked the schematic to make sure nothing is missing so far
* Fixed some wrongly assigned pin-numbers on relays

Lessons Learned:
* How to edit pre-existing components

Resources Used:
* None

## Thursday, 07-11-2019
Summary of Activities:
* Worked out the basic program-structure
* Started writing the code for the program
* Settled on number of inputs and outputs for the prototype

Lessons Learned:
* None

Resources Used:
* [Arduino](https://www.arduino.cc/reference/en/)

## Friday, 08-11-2019
Summary of Activities:
* Moved from Capture CIS to PCB Editor
* Started placing components on PCB

Lessons Learned:
* How to make sure all components are properly transferred to PCB Editor.

Resources Used:
* None

## Saturday, 09-11-2019
Summary of Activities:
* Finished placing components on PCB
* Created routes on the PCB
 
Lessons Learned:
* How to place components and route a PCB manually
 
Resources Used:
* NordCAD OrCAD Workshop PDFs (from week 43)

## Tuesday, 12-11-2019
Summary of Activities:
* Did countless DRCs (Design Rule Checks)
* Fixed issues with the schematic and PCB
* Finished PCB layout
* Created Gerber files and drill files
* Created list of components that need to be procured for the production of the prototype

Lessons Learned:
* How to make off-page connections between components and nets in Capture CIS
* How to run DRCs
* How to manually create Gerber files and drill files in PCB Editor

Resources used:
* [Digi-key](https://www.digikey.dk/)

## Friday, 15-11-2019
Summary of Activities:
* Fixed a major problem with INTA and INTB on the MCP23017s in the schematic and PCB
* Created new set of Gerber files and drill files based on the revised schematic
* Worked on programming

Lessons Learned:
* Always read available documentation for ICs used in a project before trying to design a project

Resources used:
* None

## Tuesday, 19-11-2019
Summary of Activities:
* Worked on the program for the microcontroller
* Attended a Masterclass on Design For Manufacturing

Lessons Learned:
* How to use an Arduino MCP23017-library for communication between an ATMEGA328P and an MCP23017 16-bit I2C-based Port Expander
* What to consider when designing a PCB for manufacturing

Resources used:
* [Official Documentation for the MCP23017 library used, available on GitHub](https://github.com/blemasle/arduino-mcp23017)
* [Official Documentation for built-in features in the Arduino programming-language](https://www.arduino.cc/reference/en/)
* [Official Documentation for the library for Hitachi HD44780-compatible LCD modules](https://www.arduino.cc/en/Reference/LiquidCrystal)

## Wednesday, 20-11-2019
Summary of Activities:
* Continued work on arduino program

Lessons Learned:
* Embedded comments (and in-line comments) make it a lot easier to remember what each part of the program does. This applies regardless of the size and complexity of the program.

Resources used:
* [Official Documentation for the MCP23017 library used, available on GitHub](https://github.com/blemasle/arduino-mcp23017)
* [Official Documentation for built-in features in the Arduino programming-language](https://www.arduino.cc/reference/en/)
* [Official Documentation for the library for Hitachi HD44780-compatible LCD modules](https://www.arduino.cc/en/Reference/LiquidCrystal)

## Monday, 02-12-2019
Summary of Activities:
* Received PCB
* Fixed several routing-errors on the PCB
* Started mounting components on PCB

Lessons Learned:
* To make sure footprints and schematic symbols match with the real components chosen for a project

Resources used:
* None

## Tuesday, 03-12-2019
Summary of Activities:
* Continued mounting components on PCB
* Started planning the pitch that will be given at the exam