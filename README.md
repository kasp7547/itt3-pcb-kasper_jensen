# ITT3-PCB-Kasper_Jensen

Project for 3rd Semester Elective Course - PCB Production.

# Project Overview:
The purpose of this project is to learn to use OrCAD for the main stages of electronics development. A schematic will be created, after which simulations will be run. Both of these steps will be carried out in Capture CIS.
Finally, the PCB will be created in PCB Editor, with the aim of making it as compact as possible.

# Project Goals:
* Learning to create schematics, run simulations and create PCBs in OrCAD
* Creating an expandable Pedalboard Controller using an Arduino UNO (or ATMEGA328P chip)

# Project Specifications:
The product will be built around an ATMEGA328P chip or an Arduino UNO. To expedite the development on the programming side, the code will be written in Arduino, but may later be adapted to Embedded C.
Since more pins are needed than the ATMEGA328P has, a number of Arduino-compatible Port Expanders (MCP23017 or similar) will be used to increase the number of pins available.
The exact number of pins will be determined as soon as possible and will be listed in this document.

# Technical Requirements:
* Individually switchable loops
* Momentary NO (Normally Open) footswitches
* Tuner-Out (3PDT Switch)
* 5 Presets per bank (ideally at least 10 banks)
* LEDs to indicate which preset and which individual loops are active
* 7-Segment display to show active Preset Bank number
* Hitachi HD44780-compatible LCD-display (2x16)
* OPTIONAL: Loop for input-buffer
* OPTIONAL: Compatibility with Stereo-In and Stereo-Out pedals
* OPTIONAL: Possibility to switch the order of individual effects (Advanced)
* OPTIONAL: Possibility of signal-splitting (Advanced)

A demo-version with a lower number of loops is being developed as proof of concept, which may later be developed into a finalized, marketable product.

The demo-version will only have 1 Preset Bank consisting of 5 Presets, while the finalized version will feature multiple banks.

# Help Required:
I will need to do some technical sparring with the teacher, to clear up a few things about the project idea.


***Project Status***
At the moment, the product does not work. However, a concept-prototype will be
demonstrated at the presentation, with a few features cut out.

A functional version of the full project will still be developed.