The OrCAD files related to the project can be found in this folder.

PEDALBOARD_LIB.olb is a library containing custom parts used in the project.
Pedalboard_switcher.opj is the OrCAD Project file itself, which contains the schematics, netlist, Bill Of Materials and more.